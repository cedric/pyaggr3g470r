# Translations template for PROJECT.
# Copyright (C) 2015 ORGANIZATION
# This file is distributed under the same license as the PROJECT project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2015-08-05 15:24+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.0\n"

#: pyaggr3g470r/forms.py:45 pyaggr3g470r/forms.py:113 pyaggr3g470r/forms.py:136
#: pyaggr3g470r/templates/admin/dashboard.html:12
msgid "Nickname"
msgstr ""

#: pyaggr3g470r/forms.py:46 pyaggr3g470r/forms.py:114 pyaggr3g470r/forms.py:137
msgid "Please enter your nickname."
msgstr ""

#: pyaggr3g470r/forms.py:47 pyaggr3g470r/forms.py:115 pyaggr3g470r/forms.py:138
#: pyaggr3g470r/forms.py:186 pyaggr3g470r/templates/admin/dashboard.html:13
msgid "Email"
msgstr ""

#: pyaggr3g470r/forms.py:50 pyaggr3g470r/forms.py:86 pyaggr3g470r/forms.py:189
msgid "Please enter your email address."
msgstr ""

#: pyaggr3g470r/forms.py:51 pyaggr3g470r/forms.py:87 pyaggr3g470r/forms.py:118
#: pyaggr3g470r/forms.py:141
msgid "Password"
msgstr ""

#: pyaggr3g470r/forms.py:52 pyaggr3g470r/forms.py:88
msgid "Please enter a password."
msgstr ""

#: pyaggr3g470r/forms.py:55 pyaggr3g470r/templates/login.html:26
msgid "Sign up"
msgstr ""

#: pyaggr3g470r/forms.py:60 pyaggr3g470r/forms.py:127 pyaggr3g470r/forms.py:156
msgid ""
"This nickname has invalid characters. Please use letters, numbers, dots "
"and underscores only."
msgstr ""

#: pyaggr3g470r/forms.py:90 pyaggr3g470r/templates/login.html:5
msgid "Log In"
msgstr ""

#: pyaggr3g470r/forms.py:101
msgid "Account not confirmed"
msgstr ""

#: pyaggr3g470r/forms.py:104
msgid "Invalid email or password"
msgstr ""

#: pyaggr3g470r/forms.py:117 pyaggr3g470r/forms.py:140
msgid "Please enter your email."
msgstr ""

#: pyaggr3g470r/forms.py:119 pyaggr3g470r/forms.py:143
msgid "Feeds refresh frequency (in minutes)"
msgstr ""

#: pyaggr3g470r/forms.py:122 pyaggr3g470r/forms.py:146
#: pyaggr3g470r/forms.py:169
msgid "Save"
msgstr ""

#: pyaggr3g470r/forms.py:142
msgid "Password Confirmation"
msgstr ""

#: pyaggr3g470r/forms.py:151
msgid "Passwords aren't the same."
msgstr ""

#: pyaggr3g470r/forms.py:164 pyaggr3g470r/templates/feeds.html:11
#: pyaggr3g470r/templates/layout.html:106
#: pyaggr3g470r/templates/admin/user.html:26
msgid "Title"
msgstr ""

#: pyaggr3g470r/forms.py:165 pyaggr3g470r/templates/admin/user.html:27
msgid "Feed link"
msgstr ""

#: pyaggr3g470r/forms.py:166
msgid "Please enter the URL."
msgstr ""

#: pyaggr3g470r/forms.py:167 pyaggr3g470r/templates/admin/user.html:28
msgid "Site link"
msgstr ""

#: pyaggr3g470r/forms.py:168
msgid "Check for updates"
msgstr ""

#: pyaggr3g470r/forms.py:178
msgid "Subject"
msgstr ""

#: pyaggr3g470r/forms.py:179
msgid "Please enter a subject."
msgstr ""

#: pyaggr3g470r/forms.py:180
msgid "Message"
msgstr ""

#: pyaggr3g470r/forms.py:181
msgid "Please enter a content."
msgstr ""

#: pyaggr3g470r/forms.py:182
msgid "Send"
msgstr ""

#: pyaggr3g470r/forms.py:190
msgid "Recover"
msgstr ""

#: pyaggr3g470r/forms.py:200
msgid "Account not confirmed."
msgstr ""

#: pyaggr3g470r/forms.py:203
msgid "Invalid email."
msgstr ""

#: pyaggr3g470r/templates/about.html:5 pyaggr3g470r/templates/layout.html:85
#: pyaggr3g470r/templates/layout.html:118
msgid "About"
msgstr ""

#: pyaggr3g470r/templates/about.html:7
msgid ""
"pyAggr3g470r is a news aggregator platform and can be shared between "
"several users."
msgstr ""

#: pyaggr3g470r/templates/about.html:8
msgid "You can easily install pyAggr3g470r on your server."
msgstr ""

#: pyaggr3g470r/templates/about.html:9
msgid "Alternatively, you can deploy your own copy using this button:"
msgstr ""

#: pyaggr3g470r/templates/about.html:11
msgid ""
"This software is under AGPLv3 license. You are welcome to copy, modify or"
"\n"
"        redistribute the <a "
"href=\"https://bitbucket.org/cedricbonhomme/pyaggr3g470r\">source "
"code</a>\n"
"        according to the <a "
"href=\"https://www.gnu.org/licenses/agpl-3.0.html\">Affero GPL</a> "
"license."
msgstr ""

#: pyaggr3g470r/templates/about.html:14
msgid ""
"Found a bug? Report it <a "
"href=\"https://bitbucket.org/cedricbonhomme/pyaggr3g470r/issues\">here</a>."
msgstr ""

#: pyaggr3g470r/templates/about.html:17
msgid "Help"
msgstr ""

#: pyaggr3g470r/templates/about.html:18
msgid ""
"If you have any problem, <a "
"href=\"http://wiki.cedricbonhomme.org/contact\">contact</a> the "
"administrator."
msgstr ""

#: pyaggr3g470r/templates/about.html:19
msgid ""
"The documentation of the RESTful API is <a "
"href=\"https://pyaggr3g470r.readthedocs.org/en/latest/web-"
"services.html\">here</a>."
msgstr ""

#: pyaggr3g470r/templates/about.html:20
msgid ""
"You can subscribe to new feeds with a bookmarklet. Drag the following "
"button to your browser bookmarks."
msgstr ""

#: pyaggr3g470r/templates/about.html:21
#, python-format
msgid ""
"<a class=\"btn btn-default\" href=\"%(bookmarklet)s\" "
"rel=\"bookmark\">Subscribe to this feed using pyAggr3g470r</a>"
msgstr ""

#: pyaggr3g470r/templates/about.html:24
msgid "Donation"
msgstr ""

#: pyaggr3g470r/templates/about.html:25
msgid ""
"If you wish and if you like pyAggr3g470r, you can donate via bitcoin <a "
"href=\"https://blockexplorer.com/address/1GVmhR9fbBeEh7rP1qNq76jWArDdDQ3otZ\">1GVmhR9fbBeEh7rP1qNq76jWArDdDQ3otZ</a>."
" Thank you!"
msgstr ""

#: pyaggr3g470r/templates/article.html:10
msgid "from"
msgstr ""

#: pyaggr3g470r/templates/article.html:11
#: pyaggr3g470r/templates/duplicates.html:22
#: pyaggr3g470r/templates/duplicates.html:23
#: pyaggr3g470r/templates/home.html:95
msgid "Delete this article"
msgstr ""

#: pyaggr3g470r/templates/article.html:13 pyaggr3g470r/templates/home.html:97
msgid "One of your favorites"
msgstr ""

#: pyaggr3g470r/templates/article.html:15 pyaggr3g470r/templates/home.html:99
msgid "Click if you like this article"
msgstr ""

#: pyaggr3g470r/templates/article.html:18 pyaggr3g470r/templates/home.html:102
msgid "Mark this article as unread"
msgstr ""

#: pyaggr3g470r/templates/article.html:20 pyaggr3g470r/templates/home.html:104
msgid "Mark this article as read"
msgstr ""

#: pyaggr3g470r/templates/article.html:30
msgid "Next post:"
msgstr ""

#: pyaggr3g470r/templates/article.html:33
msgid "Previous post:"
msgstr ""

#: pyaggr3g470r/templates/article.html:39
#: pyaggr3g470r/templates/article.html:42
msgid "Share on"
msgstr ""

#: pyaggr3g470r/templates/duplicates.html:4
msgid "Duplicates in the feed"
msgstr ""

#: pyaggr3g470r/templates/duplicates.html:11
#: pyaggr3g470r/templates/duplicates.html:14
msgid "Delete all in this column"
msgstr ""

#: pyaggr3g470r/templates/edit_feed.html:19
#: pyaggr3g470r/templates/edit_feed.html:27
msgid "Optional"
msgstr ""

#: pyaggr3g470r/templates/edit_feed.html:41
msgid "Filters"
msgstr ""

#: pyaggr3g470r/templates/edit_feed.html:52
msgid "simple match"
msgstr ""

#: pyaggr3g470r/templates/edit_feed.html:53
msgid "regex"
msgstr ""

#: pyaggr3g470r/templates/edit_feed.html:57
msgid "match"
msgstr ""

#: pyaggr3g470r/templates/edit_feed.html:58
msgid "no match"
msgstr ""

#: pyaggr3g470r/templates/edit_feed.html:61
msgid "mark as read"
msgstr ""

#: pyaggr3g470r/templates/edit_feed.html:62
msgid "mark as favorite"
msgstr ""

#: pyaggr3g470r/templates/feed.html:7 pyaggr3g470r/templates/feeds.html:41
#: pyaggr3g470r/templates/home.html:31 pyaggr3g470r/templates/home.html:48
#: pyaggr3g470r/templates/admin/user.html:44
msgid "Delete this feed"
msgstr ""

#: pyaggr3g470r/templates/feed.html:7 pyaggr3g470r/templates/feeds.html:41
#: pyaggr3g470r/templates/home.html:31 pyaggr3g470r/templates/home.html:48
#: pyaggr3g470r/templates/admin/user.html:44
msgid "You are going to delete this feed."
msgstr ""

#: pyaggr3g470r/templates/feed.html:8 pyaggr3g470r/templates/feeds.html:39
#: pyaggr3g470r/templates/home.html:30 pyaggr3g470r/templates/home.html:47
#: pyaggr3g470r/templates/admin/user.html:43
msgid "Edit this feed"
msgstr ""

#: pyaggr3g470r/templates/feed.html:12
msgid "This feed contains"
msgstr ""

#: pyaggr3g470r/templates/feed.html:12
msgid "articles"
msgstr ""

#: pyaggr3g470r/templates/feed.html:13
msgid "Address of the feed"
msgstr ""

#: pyaggr3g470r/templates/feed.html:15
msgid "Address of the site"
msgstr ""

#: pyaggr3g470r/templates/feed.html:21
msgid "Last download:"
msgstr ""

#: pyaggr3g470r/templates/feed.html:25
msgid ""
"That feed has encountered too much consecutive errors and won't be "
"retrieved anymore."
msgstr ""

#: pyaggr3g470r/templates/feed.html:26
#, python-format
msgid ""
"You can click <a href='%(reset_error_url)s'>here</a> to reset the error "
"count and reactivate the feed."
msgstr ""

#: pyaggr3g470r/templates/feed.html:28
msgid ""
"The download of this feed has encountered some problems. However its "
"error counter will be reinitialized at the next successful retrieving."
msgstr ""

#: pyaggr3g470r/templates/feed.html:32
msgid "Here's the last error encountered while retrieving this feed:"
msgstr ""

#: pyaggr3g470r/templates/feed.html:36
msgid "The last article was posted"
msgstr ""

#: pyaggr3g470r/templates/feed.html:36
msgid "day(s) ago."
msgstr ""

#: pyaggr3g470r/templates/feed.html:37
msgid "Daily average"
msgstr ""

#: pyaggr3g470r/templates/feed.html:37
msgid "between the"
msgstr ""

#: pyaggr3g470r/templates/feed.html:37
msgid "and the"
msgstr ""

#: pyaggr3g470r/templates/feeds.html:4 pyaggr3g470r/templates/management.html:6
msgid "You are subscribed to"
msgstr ""

#: pyaggr3g470r/templates/feeds.html:4 pyaggr3g470r/templates/management.html:6
msgid "feeds"
msgstr ""

#: pyaggr3g470r/templates/feeds.html:4 pyaggr3g470r/templates/management.html:6
msgid "Add a"
msgstr ""

#: pyaggr3g470r/templates/feeds.html:4 pyaggr3g470r/templates/management.html:6
msgid "feed"
msgstr ""

#: pyaggr3g470r/templates/feeds.html:10
msgid "Status"
msgstr ""

#: pyaggr3g470r/templates/feeds.html:12
msgid "Site"
msgstr ""

#: pyaggr3g470r/templates/feeds.html:13 pyaggr3g470r/templates/feeds.html:38
msgid "Articles"
msgstr ""

#: pyaggr3g470r/templates/feeds.html:14
#: pyaggr3g470r/templates/admin/dashboard.html:15
#: pyaggr3g470r/templates/admin/user.html:30
msgid "Actions"
msgstr ""

#: pyaggr3g470r/templates/feeds.html:23
msgid "Feed enabled"
msgstr ""

#: pyaggr3g470r/templates/feeds.html:25
msgid "Feed disabled"
msgstr ""

#: pyaggr3g470r/templates/feeds.html:28
msgid "Feed encountered too much errors."
msgstr ""

#: pyaggr3g470r/templates/feeds.html:40
msgid "Duplicate articles"
msgstr ""

#: pyaggr3g470r/templates/history.html:4 pyaggr3g470r/templates/layout.html:74
msgid "History"
msgstr ""

#: pyaggr3g470r/templates/history.html:9
msgid "all years"
msgstr ""

#: pyaggr3g470r/templates/home.html:5
msgid "You don't have any feeds."
msgstr ""

#: pyaggr3g470r/templates/home.html:6
msgid "Add some"
msgstr ""

#: pyaggr3g470r/templates/home.html:6 pyaggr3g470r/templates/management.html:13
msgid "or"
msgstr ""

#: pyaggr3g470r/templates/home.html:6
msgid "upload an OPML file."
msgstr ""

#: pyaggr3g470r/templates/home.html:15
msgid "All feeds"
msgstr ""

#: pyaggr3g470r/templates/home.html:22 pyaggr3g470r/templates/home.html:39
msgid "error"
msgstr ""

#: pyaggr3g470r/templates/home.html:29 pyaggr3g470r/templates/home.html:46
msgid "Details"
msgstr ""

#: pyaggr3g470r/templates/home.html:32 pyaggr3g470r/templates/home.html:49
msgid "Mark this feed as read"
msgstr ""

#: pyaggr3g470r/templates/home.html:33 pyaggr3g470r/templates/home.html:50
msgid "Mark this feed as unread"
msgstr ""

#: pyaggr3g470r/templates/home.html:68 pyaggr3g470r/templates/home.html:76
#: pyaggr3g470r/templates/layout.html:75
msgid "All"
msgstr ""

#: pyaggr3g470r/templates/home.html:69
msgid "Read"
msgstr ""

#: pyaggr3g470r/templates/home.html:70
msgid "Unread"
msgstr ""

#: pyaggr3g470r/templates/home.html:86 pyaggr3g470r/templates/layout.html:67
#: pyaggr3g470r/templates/admin/user.html:42
msgid "Feed"
msgstr ""

#: pyaggr3g470r/templates/home.html:87 pyaggr3g470r/views/views.py:418
msgid "Article"
msgstr ""

#: pyaggr3g470r/templates/home.html:88
msgid "Date"
msgstr ""

#: pyaggr3g470r/templates/home.html:113
msgid "No icon found for this feed"
msgstr ""

#: pyaggr3g470r/templates/inactives.html:6
msgid "Days of inactivity"
msgstr ""

#: pyaggr3g470r/templates/inactives.html:17
msgid "days"
msgstr ""

#: pyaggr3g470r/templates/inactives.html:22
msgid "No inactive feeds."
msgstr ""

#: pyaggr3g470r/templates/layout.html:40 pyaggr3g470r/templates/layout.html:43
#: pyaggr3g470r/views/feed.py:152
msgid "Add a feed"
msgstr ""

#: pyaggr3g470r/templates/layout.html:49
msgid "site or feed url"
msgstr ""

#: pyaggr3g470r/templates/layout.html:59
msgid "Home"
msgstr ""

#: pyaggr3g470r/templates/layout.html:61 pyaggr3g470r/views/views.py:315
msgid "Favorites"
msgstr ""

#: pyaggr3g470r/templates/layout.html:64
msgid "Fetch"
msgstr ""

#: pyaggr3g470r/templates/layout.html:69
msgid "Mark all as read"
msgstr ""

#: pyaggr3g470r/templates/layout.html:70
msgid "Mark all as read older than yesterday"
msgstr ""

#: pyaggr3g470r/templates/layout.html:71
msgid "Mark all as read older than 10 days"
msgstr ""

#: pyaggr3g470r/templates/layout.html:73
msgid "Inactive"
msgstr ""

#: pyaggr3g470r/templates/layout.html:83
msgid "Profile"
msgstr ""

#: pyaggr3g470r/templates/layout.html:84
msgid "Your data"
msgstr ""

#: pyaggr3g470r/templates/layout.html:88
msgid "Dashboard"
msgstr ""

#: pyaggr3g470r/templates/layout.html:91
msgid "Logout"
msgstr ""

#: pyaggr3g470r/templates/layout.html:109
msgid "Content"
msgstr ""

#: pyaggr3g470r/templates/layout.html:111
msgid "Search"
msgstr ""

#: pyaggr3g470r/templates/login.html:10 pyaggr3g470r/templates/recover.html:12
msgid "Your email"
msgstr ""

#: pyaggr3g470r/templates/login.html:17
msgid "Your Password"
msgstr ""

#: pyaggr3g470r/templates/login.html:28
msgid "Forgot password"
msgstr ""

#: pyaggr3g470r/templates/management.html:5
msgid "Your subscriptions"
msgstr ""

#: pyaggr3g470r/templates/management.html:7
msgid "articles are stored in the database with"
msgstr ""

#: pyaggr3g470r/templates/management.html:7
msgid "unread articles"
msgstr ""

#: pyaggr3g470r/templates/management.html:8
msgid "You are going to delete old articles."
msgstr ""

#: pyaggr3g470r/templates/management.html:8
msgid "Delete articles older than 10 weeks"
msgstr ""

#: pyaggr3g470r/templates/management.html:11
msgid "OPML import/export"
msgstr ""

#: pyaggr3g470r/templates/management.html:13
msgid "Batch import feeds from OPML"
msgstr ""

#: pyaggr3g470r/templates/management.html:17
msgid "Export feeds to OPML"
msgstr ""

#: pyaggr3g470r/templates/management.html:18
msgid "Data liberation"
msgstr ""

#: pyaggr3g470r/templates/management.html:20
msgid "Import account"
msgstr ""

#: pyaggr3g470r/templates/management.html:24
msgid "Export account to JSON"
msgstr ""

#: pyaggr3g470r/templates/management.html:27
msgid "Export articles"
msgstr ""

#: pyaggr3g470r/templates/profile.html:5
msgid "Your Profile"
msgstr ""

#: pyaggr3g470r/templates/profile.html:8
#: pyaggr3g470r/templates/admin/user.html:12
msgid "Member since"
msgstr ""

#: pyaggr3g470r/templates/profile.html:9
#: pyaggr3g470r/templates/admin/user.html:13
msgid "Last seen:"
msgstr ""

#: pyaggr3g470r/templates/profile.html:14
msgid "You are going to delete your account."
msgstr ""

#: pyaggr3g470r/templates/profile.html:14
msgid "Delete your account"
msgstr ""

#: pyaggr3g470r/templates/recover.html:5
msgid "Recover your account"
msgstr ""

#: pyaggr3g470r/templates/signup.html:10
msgid "Letters, numbers, dots and underscores only."
msgstr ""

#: pyaggr3g470r/templates/signup.html:17
msgid "Minimum 6 characters."
msgstr ""

#: pyaggr3g470r/templates/admin/dashboard.html:7
msgid "Registered users"
msgstr ""

#: pyaggr3g470r/templates/admin/dashboard.html:14
msgid "Last seen"
msgstr ""

#: pyaggr3g470r/templates/admin/dashboard.html:30
msgid "View this user"
msgstr ""

#: pyaggr3g470r/templates/admin/dashboard.html:31
#: pyaggr3g470r/templates/admin/user.html:8
msgid "Edit this user"
msgstr ""

#: pyaggr3g470r/templates/admin/dashboard.html:38
msgid "Delete this user"
msgstr ""

#: pyaggr3g470r/templates/admin/dashboard.html:38
msgid "You are going to delete this account."
msgstr ""

#: pyaggr3g470r/templates/admin/dashboard.html:45
#: pyaggr3g470r/views/views.py:733
msgid "Add a new user"
msgstr ""

#: pyaggr3g470r/templates/admin/dashboard.html:46
msgid "Send notification messages"
msgstr ""

#: pyaggr3g470r/templates/admin/user.html:9
msgid "Membership"
msgstr ""

#: pyaggr3g470r/templates/admin/user.html:19
msgid "This user is not subscribed to any feed."
msgstr ""

#: pyaggr3g470r/templates/admin/user.html:21
msgid "Feeds"
msgstr ""

#: pyaggr3g470r/templates/admin/user.html:29
msgid "(unread) articles"
msgstr ""

#: pyaggr3g470r/views/feed.py:78
#, python-format
msgid "Feed %(feed_title)s successfully deleted."
msgstr ""

#: pyaggr3g470r/views/feed.py:89 pyaggr3g470r/views/feed.py:194
#, python-format
msgid "Feed %(feed_title)r successfully updated."
msgstr ""

#: pyaggr3g470r/views/feed.py:100
msgid "Couldn't add feed: url missing."
msgstr ""

#: pyaggr3g470r/views/feed.py:105 pyaggr3g470r/views/feed.py:178
msgid "Couldn't add feed: feed already exists."
msgstr ""

#: pyaggr3g470r/views/feed.py:112
msgid "Impossible to connect to the address: {}."
msgstr ""

#: pyaggr3g470r/views/feed.py:117
msgid ""
"Couldn't find a feed url, you'll need to find a Atom or RSS link manually"
" and reactivate this feed"
msgstr ""

#: pyaggr3g470r/views/feed.py:121
msgid "Feed was successfully created."
msgstr ""

#: pyaggr3g470r/views/feed.py:124 pyaggr3g470r/views/feed.py:206
msgid "Downloading articles for the new feed..."
msgstr ""

#: pyaggr3g470r/views/feed.py:143
msgid "Feed successfully updated."
msgstr ""

#: pyaggr3g470r/views/feed.py:158
msgid "Edit feed"
msgstr ""

#: pyaggr3g470r/views/feed.py:201
#, python-format
msgid "Feed %(feed_title)r successfully created."
msgstr ""

#: pyaggr3g470r/views/views.py:69 pyaggr3g470r/views/views.py:111
msgid "Authentication required."
msgstr ""

#: pyaggr3g470r/views/views.py:116
msgid "Forbidden."
msgstr ""

#: pyaggr3g470r/views/views.py:189
msgid "Logged out successfully."
msgstr ""

#: pyaggr3g470r/views/views.py:198
msgid "Self-registration is disabled."
msgstr ""

#: pyaggr3g470r/views/views.py:215
msgid "Email already used."
msgstr ""

#: pyaggr3g470r/views/views.py:222
#, python-format
msgid "Problem while sending activation email: %(error)s"
msgstr ""

#: pyaggr3g470r/views/views.py:226
msgid "Your account has been created. Check your mail to confirm it."
msgstr ""

#: pyaggr3g470r/views/views.py:323
msgid "No text to search were provided."
msgstr ""

#: pyaggr3g470r/views/views.py:338
msgid "Search:"
msgstr ""

#: pyaggr3g470r/views/views.py:354 pyaggr3g470r/views/views.py:527
msgid "Downloading articles..."
msgstr ""

#: pyaggr3g470r/views/views.py:356
msgid ""
"The manual retrieving of news is only available for administrator, on the"
" Heroku platform."
msgstr ""

#: pyaggr3g470r/views/views.py:418
msgid "deleted."
msgstr ""

#: pyaggr3g470r/views/views.py:421
msgid "This article do not exist."
msgstr ""

#: pyaggr3g470r/views/views.py:456
msgid "No duplicates in the feed \"{}\"."
msgstr ""

#: pyaggr3g470r/views/views.py:473 pyaggr3g470r/views/views.py:484
msgid "Error when exporting articles."
msgstr ""

#: pyaggr3g470r/views/views.py:490
msgid "Export format not supported."
msgstr ""

#: pyaggr3g470r/views/views.py:519 pyaggr3g470r/views/views.py:535
#: pyaggr3g470r/views/views.py:544
msgid "File not allowed."
msgstr ""

#: pyaggr3g470r/views/views.py:525
msgid "feeds imported."
msgstr ""

#: pyaggr3g470r/views/views.py:529
msgid "Impossible to import the new feeds."
msgstr ""

#: pyaggr3g470r/views/views.py:539
msgid "Account imported."
msgstr ""

#: pyaggr3g470r/views/views.py:541
msgid "Impossible to import the account."
msgstr ""

#: pyaggr3g470r/views/views.py:581 pyaggr3g470r/views/views.py:711
#: pyaggr3g470r/views/views.py:721 pyaggr3g470r/views/views.py:766
msgid "User"
msgstr ""

#: pyaggr3g470r/views/views.py:582 pyaggr3g470r/views/views.py:711
msgid "successfully updated."
msgstr ""

#: pyaggr3g470r/views/views.py:602
msgid "Your account has been deleted."
msgstr ""

#: pyaggr3g470r/views/views.py:604 pyaggr3g470r/views/views.py:752
#: pyaggr3g470r/views/views.py:768 pyaggr3g470r/views/views.py:796
msgid "This user does not exist."
msgstr ""

#: pyaggr3g470r/views/views.py:621
msgid "Articles deleted."
msgstr ""

#: pyaggr3g470r/views/views.py:635
msgid "Your account has been confirmed."
msgstr ""

#: pyaggr3g470r/views/views.py:637
msgid "Impossible to confirm this account."
msgstr ""

#: pyaggr3g470r/views/views.py:659
msgid "New password sent to your address."
msgstr ""

#: pyaggr3g470r/views/views.py:661
msgid "Problem while sending your new password."
msgstr ""

#: pyaggr3g470r/views/views.py:686
msgid "Problem while sending email"
msgstr ""

#: pyaggr3g470r/views/views.py:721
msgid "successfully created."
msgstr ""

#: pyaggr3g470r/views/views.py:730
msgid "Edit the user"
msgstr ""

#: pyaggr3g470r/views/views.py:766
msgid "successfully deleted."
msgstr ""

#: pyaggr3g470r/views/views.py:787 pyaggr3g470r/views/views.py:793
msgid "Account of the user"
msgstr ""

#: pyaggr3g470r/views/views.py:787
msgid "successfully activated."
msgstr ""

#: pyaggr3g470r/views/views.py:789
msgid "Problem while sending activation email"
msgstr ""

#: pyaggr3g470r/views/views.py:793
msgid "successfully disabled."
msgstr ""

